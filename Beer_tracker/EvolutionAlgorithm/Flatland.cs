﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvolutionAlgorithm
{
    public class Flatland
    {
        public int width = 30;
        public int height = 15;
        public int objectPositionX;
        public int objectPositionY;
        public int objectWidth;
        public bool objectExists;

        public Flatland()
        {
            objectExists = false;
        }

        public void makeNewObject()
        {
            objectExists = true;
            objectWidth=Program.rnd.Next(1,7);
            objectPositionY = 0;
            objectPositionX = Program.rnd.Next(width - objectWidth);
        }

        internal void restartFlatland()
        {
            objectExists = true;
            objectWidth = Program.rnd.Next(1,7);
            objectPositionY = 0;
            objectPositionX = Program.rnd.Next(width - objectWidth);
        }

        public void pullObject()
        {
            objectPositionY = height-1;
        }
    }
}

