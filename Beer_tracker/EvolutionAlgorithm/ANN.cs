﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EvolutionAlgorithm.Phenotype;

namespace EvolutionAlgorithm
{
    public partial class ANN : Form
    {
        public Population population;
        public Agent agent;
        public bool drawing = false;

        //chart
        public string stringAverageFitness = "Average Fitness";
        public string stringdeviation = "Standard Deviation";
        public string stringMaxFitness = "Max Fitness";
        //chart

        public ANN()
        {
            InitializeComponent();
            cmbAdultSelection.SelectedIndex = 2;
            cmbMateSelection.SelectedIndex = 2;
        }

        private void Initialize()
        {
            //chart
            chart1.Series[stringAverageFitness].Points.Clear();
            chart1.Series[stringMaxFitness].Points.Clear();
            chart1.Series[stringdeviation].Points.Clear();
            chart1.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
            chart1.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
            //chart


            int weightBitLenght;
            int populationSize;
            Program.elitism = true;

            try
            {
                weightBitLenght = int.Parse(txtWeightBitLength.Text);
                populationSize = int.Parse(txtPopulationSize.Text);
                Program.mutationRate = double.Parse(txtMutationRate.Text);
                Program.crossOverRate = double.Parse(txtCrossOverRate.Text);
                Program.tournamentSelectionRate = double.Parse(txtTournamentSelectionRate.Text);
            }
            catch (Exception)
            {
                weightBitLenght = 10;
                populationSize = 50;
                Program.mutationRate = 0.001;
                Program.crossOverRate = 0.8;
                Program.tournamentSelectionRate = 0.2;
            }

            agent = new Agent();
            agent.wallWrap = chbWallWrap.Checked;
            agent.pull = chbPull.Checked;
            population = new Population(weightBitLenght,
                (Enums.AdultSelection)cmbAdultSelection.SelectedIndex,
                (Enums.ParentSelection)cmbMateSelection.SelectedIndex, populationSize);
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void btnBegin_Click(object sender, EventArgs e)
        {
            Initialize();
            while (population.generation < 500)
            {
                population.Reproduce();

                //chart
                chart1.Series[stringMaxFitness].Points.AddXY(population.generation,
                    population.maxFitness[population.generation - 1]);
                chart1.Series[stringdeviation].Points.AddXY(population.generation,
                    population.standardDeviation[population.generation - 1]);
                chart1.Series[stringAverageFitness].Points.AddXY(population.generation,
                    population.averageFitness[population.generation - 1]);
                //chart
            }
            lblSteps.Text = "You can test the best one!";
            agent.restartPosition();
            this.Refresh();
        }

        private void ANN_Load(object sender, EventArgs e)
        {

        }

        protected override void OnPaint(PaintEventArgs e)
        {
            System.Drawing.Pen pen = new System.Drawing.Pen(Color.LightGray, 1);
            System.Drawing.SolidBrush brush = new System.Drawing.SolidBrush(System.Drawing.Color.Gray);
            System.Drawing.Graphics formGraphics;
            formGraphics = this.CreateGraphics();

            int gridSize = 15;
            int stepX = gridPanel.Width / gridSize / 2;
            int stepY = gridPanel.Height / gridSize;
            //prvo ide presek, pa grid, pa agent i object
            
            for (int i = 0; i < 2 * gridSize + 1; i++)
            {
                //vertikalna
                formGraphics.DrawLine(pen, gridPanel.Location.X + i * stepX, gridPanel.Location.Y,
                gridPanel.Location.X + i * stepX, gridPanel.Location.Y + gridPanel.Height / gridSize * gridSize);

                //horizontalna
                if (i < gridSize + 1)
                {
                    formGraphics.DrawLine(pen, gridPanel.Location.X, gridPanel.Location.Y + i * stepY,
                        gridPanel.Location.X + gridPanel.Width / gridSize * gridSize, gridPanel.Location.Y + i * stepY);
                }

            }

            if (!drawing)
            {
                pen.Dispose();
                formGraphics.Dispose();
                return;
            }

            for (int i = 0; i < agent.agentLenght; i++)
            {//preseci
                int tilePosition = (agent.positionX + i + agent.flatland.width) % agent.flatland.width;
                if (agent.flatland.objectPositionX <= tilePosition &&
                    tilePosition <= agent.flatland.objectPositionX + agent.flatland.objectWidth - 1)
                {
                    formGraphics.FillRectangle(brush, new Rectangle(gridPanel.Location.X + tilePosition * stepX, gridPanel.Location.Y + (gridSize - 1) * stepY, stepX, stepY));
                }
            }

            //agent
            pen.Width = 3;
            pen.Color = Color.Black;
            for (int i = 0; i < 6; i++)
            {//vertikalna
                formGraphics.DrawLine(pen, gridPanel.Location.X + (agent.positionX + i + gridSize * 2) % (gridSize * 2) * stepX,
                    gridPanel.Location.Y + (gridSize - 1) * stepY, gridPanel.Location.X + (agent.positionX + i + gridSize * 2) % (gridSize * 2) * stepX,
                    gridPanel.Location.Y + gridSize * stepY);
            }
            if (agent.positionX + agent.agentLenght > gridSize * 2)
            {
                formGraphics.DrawLine(pen, gridPanel.Location.X + gridSize * 2 * stepX,
                    gridPanel.Location.Y + (gridSize - 1) * stepY, gridPanel.Location.X + (gridSize * 2) * stepX,
                    gridPanel.Location.Y + gridSize * stepY);
            }
            for (int i = 0; i < 5; i++)
            {//vertikalna
                if (agent.positionX + i == 29)
                {
                    formGraphics.DrawLine(pen, gridPanel.Location.X + (agent.positionX + i + (gridSize * 2)) % (gridSize * 2) * stepX,
                    gridPanel.Location.Y + (gridSize - 1) * stepY, gridPanel.Location.X + (agent.positionX + i + 1) * stepX,
                    gridPanel.Location.Y + (gridSize - 1) * stepY);
                    formGraphics.DrawLine(pen, gridPanel.Location.X + (agent.positionX + i + (gridSize * 2)) % (gridSize * 2) * stepX,
                    gridPanel.Location.Y + gridSize * stepY, gridPanel.Location.X + (agent.positionX + i + 1) * stepX,
                    gridPanel.Location.Y + gridSize * stepY);
                }
                else
                {
                    formGraphics.DrawLine(pen, gridPanel.Location.X + (agent.positionX + i + (gridSize * 2)) % (gridSize * 2) * stepX,
                    gridPanel.Location.Y + (gridSize - 1) * stepY, gridPanel.Location.X + (agent.positionX + i + 1 + (gridSize * 2)) % (gridSize * 2) * stepX,
                    gridPanel.Location.Y + (gridSize - 1) * stepY);
                    formGraphics.DrawLine(pen, gridPanel.Location.X + (agent.positionX + i + (gridSize * 2)) % (gridSize * 2) * stepX,
                    gridPanel.Location.Y + gridSize * stepY, gridPanel.Location.X + (agent.positionX + i + 1 + (gridSize * 2)) % (gridSize * 2) * stepX,
                    gridPanel.Location.Y + gridSize * stepY);
                }

            }

            //object
            pen.Width = 3;
            pen.Color = Color.Black;
            if (agent.flatland.objectExists)
            {
                for (int i = 0; i < agent.flatland.objectWidth + 1; i++)
                {//vertikalna
                    formGraphics.DrawLine(pen, gridPanel.Location.X + (agent.flatland.objectPositionX + i) * stepX,
                        gridPanel.Location.Y + agent.flatland.objectPositionY * stepY, gridPanel.Location.X + (agent.flatland.objectPositionX + i) * stepX,
                        gridPanel.Location.Y + (agent.flatland.objectPositionY + 1) * stepY);
                }

                //horizontalne
                formGraphics.DrawLine(pen, gridPanel.Location.X + agent.flatland.objectPositionX * stepX,
                gridPanel.Location.Y + agent.flatland.objectPositionY * stepY, gridPanel.Location.X + (agent.flatland.objectPositionX + agent.flatland.objectWidth) * stepX,
                gridPanel.Location.Y + agent.flatland.objectPositionY * stepY);
                formGraphics.DrawLine(pen, gridPanel.Location.X + agent.flatland.objectPositionX * stepX,
                gridPanel.Location.Y + (agent.flatland.objectPositionY + 1) * stepY, gridPanel.Location.X + (agent.flatland.objectPositionX + agent.flatland.objectWidth) * stepX,
                gridPanel.Location.Y + (agent.flatland.objectPositionY + 1) * stepY);


            }
            pen.Dispose();
            formGraphics.Dispose();
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            agent.weights = (Ph_ANN)population.children[0];
            agent.weights.updateWeights();
            agent.restartPosition();
            drawing = true;
            Refresh();
            int objectsCatched = 0;
            int objectsAvoided = 0;
            int bigObjects = 0;
            int smallObjects = 0;

            for (int i = 0; i < 600; i++)
            {
                lblSteps.Text = "Step: " + i + ", Catches: " + objectsCatched + "/" + smallObjects + ", Avoidings: " + objectsAvoided + "/" + bigObjects;
                agent.flatland.objectPositionY++;
                agent.move(agent.setDirections());
                if (agent.flatland.objectExists && agent.flatland.objectPositionY == agent.flatland.height - 1)
                {
                    if (agent.flatland.objectWidth > 4)
                    {
                        bigObjects++;
                    }
                    else
                    {
                        smallObjects++;
                    }
                    if (agent.flatland.objectWidth < 5 && agent.objectOverleps())
                    {
                        objectsCatched++;
                    }
                    else
                    {
                        if (agent.flatland.objectWidth > 4 &&
                          (agent.flatland.objectPositionX + agent.flatland.objectWidth < agent.positionX || agent.flatland.objectPositionX > agent.positionX + agent.agentLenght))
                        {
                            objectsAvoided++;
                        }
                        else
                        {
                            //punished++;
                        }

                    }
                    agent.flatland.makeNewObject();
                }

                this.Refresh();
                System.Threading.Thread.Sleep(50);
            }
            lblSteps.Text = "Done!" + " Catches: " + objectsCatched + "/" + smallObjects + ", Avoidings: " + objectsAvoided + "/" + bigObjects;
        }

        private void cmbMateSelection_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbMateSelection.SelectedIndex == 2)
            {
                txtTournamentSelectionRate.Visible = true;
                lblTournamentSelRate.Visible = true;
            }
            else
            {
                txtTournamentSelectionRate.Visible = false;
                lblTournamentSelRate.Visible = false;
            }
        }

    }
}

