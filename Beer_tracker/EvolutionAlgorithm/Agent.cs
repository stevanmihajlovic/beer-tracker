﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using EvolutionAlgorithm.Phenotype;

namespace EvolutionAlgorithm
{
    public class Agent
    {
        public int positionX;
        public int agentLenght = 5;
        public int direction = 1;
        public Ph_ANN weights;
        public int gridSize = 10;
        public Flatland flatland;
        public bool wallWrap = true;
        public bool pull;

        public Agent()
        {
            flatland = new Flatland();
            positionX = (flatland.width - agentLenght) / 2;
            flatland.restartFlatland();
        }

        public void restartPosition()
        {
            positionX = (flatland.width - agentLenght) / 2;
            flatland.restartFlatland();
        }

        public void turnLeft()
        {
            direction = -1;
        }

        public void turnRight()
        {
            direction = 1;
        }

        public int getTile(int number)
        {
            int tile = (positionX + number) % flatland.width;
            if (flatland.objectExists && flatland.objectPositionX <= tile &&
                tile < flatland.objectPositionX + flatland.objectWidth)
            {
                return 1;
            }
            return 0;
        }

        public void move(int numberOfSteps)
        {
            positionX = positionX + direction * numberOfSteps;
            if (!wallWrap)
            {
                if (positionX < 0)
                {
                    positionX = 0;
                }
                else
                {
                    if (positionX > flatland.width - agentLenght)
                    {
                        positionX = flatland.width - agentLenght;
                    }
                }
            }
            else
            {
                positionX = (positionX + flatland.width) % flatland.width;
            }
        }

        public double calculateFitness()
        {
            restartPosition();
            int maxSteps = 600;
            int objectsCatched = 0;
            int objectsAvoided = 0;
            int punished = 0;
            for (int i = 0; i < maxSteps; i++)
            {
                //object falls, than agent moves, than we catch it
                flatland.objectPositionY++;
                move(setDirections());
                if (flatland.objectExists && flatland.objectPositionY == flatland.height - 1)
                {
                    if (flatland.objectWidth < 5 && objectOverleps())
                    {
                        objectsCatched++;
                    }
                    else
                    {
                        if (flatland.objectWidth > 4 &&
                            (flatland.objectPositionX + flatland.objectWidth < positionX ||
                             flatland.objectPositionX > positionX + agentLenght))
                        {
                            objectsAvoided++;
                        }
                        else
                        {
                            punished++;
                        }

                    }
                    flatland.makeNewObject();
                }
            }
            if (wallWrap)
            {
                return (objectsCatched * 7 + objectsAvoided * 10 - punished * 3);  //7,10,3
            }
            else
            {
                return (objectsCatched * 7 + objectsAvoided * 5 - punished * 0);  //7,5,0
            }
        }

        public int setDirections()
        {
            //change direction ANN!
            double biasNode = 1;
            int weightCounter = 0;

            //input layer
            for (int i = 0; i < agentLenght; i++)
            {
                weights.layers[0].nodes[i].y = getTile(i);
            }
            if (!wallWrap)
            {
                weights.layers[0].nodes[agentLenght].y = positionX == 0 ? 1 : 0;
                weights.layers[0].nodes[agentLenght + 1].y = (positionX + agentLenght == gridSize) ? 1 : 0;
            }

            //hidden layer
            double temp;
            double[] tempWeights = new double[weights.layers[1].nodes.Length];
            for (int j = 0; j < weights.layers[1].nodes.Length; j++)
            {
                double newS = 0;
                for (int k = 0; k < weights.layers[0].nodes.Length; k++)
                {
                    temp = weights.layers[0].nodes[k].y * (weights.arrayOfWeights[weightCounter++] * 10 * (k > 4 ? 3 : 1) - 5);
                    newS += temp;
                    //previous layer
                }
                temp = biasNode * (weights.arrayOfWeights[weightCounter++] * -10);
                newS += temp;
                //bias node
                for (int k = 0; k < weights.layers[1].nodes.Length; k++)
                {
                    temp = weights.layers[1].nodes[k].calculateOutput() * (weights.arrayOfWeights[weightCounter++] * 10 - 5);
                    newS += temp;
                    //inter conections
                }

                tempWeights[j] = newS;
            }
            for (int k = 0; k < weights.layers[1].nodes.Length; k++)
            {
                weights.layers[1].nodes[k].addInput(tempWeights[k]);
            }


            //output layer
            tempWeights = new double[weights.layers[2].nodes.Length];
            for (int j = 0; j < weights.layers[2].nodes.Length; j++)
            {
                double newS = 0;
                for (int k = 0; k < weights.layers[1].nodes.Length; k++)
                {
                    temp = weights.layers[1].nodes[k].y * (weights.arrayOfWeights[weightCounter++] * 10 - 5);
                    newS += temp;
                    //previous layer
                }
                temp = biasNode * (weights.arrayOfWeights[weightCounter++] * -10);
                newS += temp;
                //bias node
                for (int k = 0; k < weights.layers[2].nodes.Length; k++)
                {
                    temp = weights.layers[2].nodes[k].calculateOutput() * (weights.arrayOfWeights[weightCounter++] * 10 - 5);
                    newS += temp;
                    //inter conections
                }

                tempWeights[j] = newS;
            }
            for (int k = 0; k < weights.layers[2].nodes.Length; k++)
            {
                weights.layers[2].nodes[k].addInput(tempWeights[k]);
            }



            double left, right;
            left = weights.layers[weights.layers.Length - 1].nodes[0].calculateOutput();
            right = weights.layers[weights.layers.Length - 1].nodes[1].calculateOutput();
            
            if (left > right)
            {
                turnLeft();

                if (left < 0.2) return 4;
                if (left < 0.3) return 3;
                if (left < 0.4) return 2;
                if (left < 0.7) return 1;
                if (pull && objectOverleps())
                {
                    flatland.objectPositionY = flatland.height - 1;
                }
                return 0;
            }
            else
            {
                turnRight();

                if (right < 0.2) return 4;
                if (right < 0.3) return 3;
                if (right < 0.4) return 2;
                if (right < 0.7) return 1;
                if (pull && objectOverleps())
                {
                    flatland.objectPositionY = flatland.height - 1;
                }
                return 0;
            }
        }


        public bool objectOverleps()
        {
            int bitObject = 0;
            int bitAgent = 0;

            for (int i = flatland.objectPositionX; i < flatland.objectPositionX + flatland.objectWidth; i++)
            {
                bitObject += 1 << i;
            }
            for (int i = 0; i < agentLenght; i++)
            {
                bitAgent += 1 << ((positionX + i) % flatland.width);
            }
            int result = bitAgent & bitObject;
            int overleps = 0;
            while (result != 0)
            {
                overleps += result % 2;
                result = result >> 1;
            }
            if (overleps == flatland.objectWidth)
            {
                return true;
            }
            return false;
        }
    }
}
