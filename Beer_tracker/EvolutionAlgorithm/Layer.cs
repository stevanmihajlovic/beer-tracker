﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EvolutionAlgorithm
{
    public class Node
    {
        public double y;
        public double t;
        public double g;
        public double s;


        public double calculateOutput()
        {
            return 1 / (1 + Math.Exp(-y * g)); //sigmoid
        }

        public void addInput(double value)
        {
            s = value;
            y = y + (-y + s) / t;
        }
    }



    public class Layer
    {
        public Node[] nodes;

        public Layer()
        {
            nodes = new Node[3];
        }

        public Layer(int numberOfNodes)
        {
            nodes = new Node[numberOfNodes];
        }
    }

}
