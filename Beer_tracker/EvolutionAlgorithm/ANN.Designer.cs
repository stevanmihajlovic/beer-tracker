﻿namespace EvolutionAlgorithm
{
    partial class ANN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend6 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series16 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series17 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series18 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblCrossOverRate = new System.Windows.Forms.Label();
            this.lblMutationRate = new System.Windows.Forms.Label();
            this.txtCrossOverRate = new System.Windows.Forms.TextBox();
            this.txtMutationRate = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtWeightBitLength = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPopulationSize = new System.Windows.Forms.TextBox();
            this.cmbMateSelection = new System.Windows.Forms.ComboBox();
            this.cmbAdultSelection = new System.Windows.Forms.ComboBox();
            this.lblTournamentSelRate = new System.Windows.Forms.Label();
            this.txtTournamentSelectionRate = new System.Windows.Forms.TextBox();
            this.btnBegin = new System.Windows.Forms.Button();
            this.gridPanel = new System.Windows.Forms.Panel();
            this.btnTest = new System.Windows.Forms.Button();
            this.lblSteps = new System.Windows.Forms.Label();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chbPull = new System.Windows.Forms.CheckBox();
            this.chbWallWrap = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 121);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(114, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Parent/mate selection:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Adult selection:";
            // 
            // lblCrossOverRate
            // 
            this.lblCrossOverRate.AutoSize = true;
            this.lblCrossOverRate.Location = new System.Drawing.Point(116, 46);
            this.lblCrossOverRate.Name = "lblCrossOverRate";
            this.lblCrossOverRate.Size = new System.Drawing.Size(78, 13);
            this.lblCrossOverRate.TabIndex = 11;
            this.lblCrossOverRate.Text = "Cross-over rate";
            // 
            // lblMutationRate
            // 
            this.lblMutationRate.AutoSize = true;
            this.lblMutationRate.Location = new System.Drawing.Point(11, 46);
            this.lblMutationRate.Name = "lblMutationRate";
            this.lblMutationRate.Size = new System.Drawing.Size(72, 13);
            this.lblMutationRate.TabIndex = 12;
            this.lblMutationRate.Text = "Mutation rate:";
            // 
            // txtCrossOverRate
            // 
            this.txtCrossOverRate.Location = new System.Drawing.Point(116, 61);
            this.txtCrossOverRate.Name = "txtCrossOverRate";
            this.txtCrossOverRate.Size = new System.Drawing.Size(100, 20);
            this.txtCrossOverRate.TabIndex = 5;
            this.txtCrossOverRate.Text = "0,9";
            // 
            // txtMutationRate
            // 
            this.txtMutationRate.Location = new System.Drawing.Point(11, 61);
            this.txtMutationRate.Name = "txtMutationRate";
            this.txtMutationRate.Size = new System.Drawing.Size(100, 20);
            this.txtMutationRate.TabIndex = 6;
            this.txtMutationRate.Text = "0,01";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(118, 7);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Weight bit length";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // txtWeightBitLength
            // 
            this.txtWeightBitLength.Location = new System.Drawing.Point(118, 22);
            this.txtWeightBitLength.Name = "txtWeightBitLength";
            this.txtWeightBitLength.Size = new System.Drawing.Size(100, 20);
            this.txtWeightBitLength.TabIndex = 7;
            this.txtWeightBitLength.Text = "8";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Population size:";
            // 
            // txtPopulationSize
            // 
            this.txtPopulationSize.Location = new System.Drawing.Point(13, 22);
            this.txtPopulationSize.Name = "txtPopulationSize";
            this.txtPopulationSize.Size = new System.Drawing.Size(100, 20);
            this.txtPopulationSize.TabIndex = 8;
            this.txtPopulationSize.Text = "15";
            // 
            // cmbMateSelection
            // 
            this.cmbMateSelection.FormattingEnabled = true;
            this.cmbMateSelection.Items.AddRange(new object[] {
            "Fitness-proportionate",
            "Sigma-scaling",
            "Tournament selection",
            "Rank Selection"});
            this.cmbMateSelection.Location = new System.Drawing.Point(11, 135);
            this.cmbMateSelection.Name = "cmbMateSelection";
            this.cmbMateSelection.Size = new System.Drawing.Size(183, 21);
            this.cmbMateSelection.TabIndex = 3;
            this.cmbMateSelection.SelectedIndexChanged += new System.EventHandler(this.cmbMateSelection_SelectedIndexChanged);
            // 
            // cmbAdultSelection
            // 
            this.cmbAdultSelection.FormattingEnabled = true;
            this.cmbAdultSelection.Items.AddRange(new object[] {
            "Full Generational Replacement",
            "Over-production",
            "Generational Mixing"});
            this.cmbAdultSelection.Location = new System.Drawing.Point(11, 98);
            this.cmbAdultSelection.Name = "cmbAdultSelection";
            this.cmbAdultSelection.Size = new System.Drawing.Size(183, 21);
            this.cmbAdultSelection.TabIndex = 4;
            // 
            // lblTournamentSelRate
            // 
            this.lblTournamentSelRate.AutoSize = true;
            this.lblTournamentSelRate.Location = new System.Drawing.Point(109, 159);
            this.lblTournamentSelRate.Name = "lblTournamentSelRate";
            this.lblTournamentSelRate.Size = new System.Drawing.Size(107, 13);
            this.lblTournamentSelRate.TabIndex = 16;
            this.lblTournamentSelRate.Text = "Tournament sel. rate:";
            this.lblTournamentSelRate.Visible = false;
            // 
            // txtTournamentSelectionRate
            // 
            this.txtTournamentSelectionRate.Location = new System.Drawing.Point(109, 173);
            this.txtTournamentSelectionRate.Name = "txtTournamentSelectionRate";
            this.txtTournamentSelectionRate.Size = new System.Drawing.Size(100, 20);
            this.txtTournamentSelectionRate.TabIndex = 15;
            this.txtTournamentSelectionRate.Text = "0,2";
            this.txtTournamentSelectionRate.Visible = false;
            // 
            // btnBegin
            // 
            this.btnBegin.Location = new System.Drawing.Point(14, 203);
            this.btnBegin.Name = "btnBegin";
            this.btnBegin.Size = new System.Drawing.Size(75, 23);
            this.btnBegin.TabIndex = 17;
            this.btnBegin.Text = "Begin";
            this.btnBegin.UseVisualStyleBackColor = true;
            this.btnBegin.Click += new System.EventHandler(this.btnBegin_Click);
            // 
            // gridPanel
            // 
            this.gridPanel.Location = new System.Drawing.Point(251, 12);
            this.gridPanel.Name = "gridPanel";
            this.gridPanel.Size = new System.Drawing.Size(600, 300);
            this.gridPanel.TabIndex = 18;
            this.gridPanel.Visible = false;
            // 
            // btnTest
            // 
            this.btnTest.Location = new System.Drawing.Point(12, 250);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(89, 23);
            this.btnTest.TabIndex = 19;
            this.btnTest.Text = "Test the best!";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // lblSteps
            // 
            this.lblSteps.AutoSize = true;
            this.lblSteps.Location = new System.Drawing.Point(9, 234);
            this.lblSteps.Name = "lblSteps";
            this.lblSteps.Size = new System.Drawing.Size(32, 13);
            this.lblSteps.TabIndex = 20;
            this.lblSteps.Text = "Step:";
            // 
            // chart1
            // 
            chartArea6.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea6);
            this.chart1.DataSource = this.chart1.Legends;
            legend6.Name = "Legend1";
            this.chart1.Legends.Add(legend6);
            this.chart1.Location = new System.Drawing.Point(16, 318);
            this.chart1.Name = "chart1";
            series16.ChartArea = "ChartArea1";
            series16.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series16.Color = System.Drawing.Color.Red;
            series16.Legend = "Legend1";
            series16.Name = "Max Fitness";
            series17.ChartArea = "ChartArea1";
            series17.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series17.Color = System.Drawing.Color.Blue;
            series17.Legend = "Legend1";
            series17.Name = "Standard Deviation";
            series18.ChartArea = "ChartArea1";
            series18.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series18.Color = System.Drawing.Color.Lime;
            series18.Legend = "Legend1";
            series18.Name = "Average Fitness";
            this.chart1.Series.Add(series16);
            this.chart1.Series.Add(series17);
            this.chart1.Series.Add(series18);
            this.chart1.Size = new System.Drawing.Size(835, 279);
            this.chart1.TabIndex = 22;
            this.chart1.Text = "chart1";
            // 
            // chbPull
            // 
            this.chbPull.AutoSize = true;
            this.chbPull.Location = new System.Drawing.Point(14, 165);
            this.chbPull.Name = "chbPull";
            this.chbPull.Size = new System.Drawing.Size(43, 17);
            this.chbPull.TabIndex = 23;
            this.chbPull.Text = "Pull";
            this.chbPull.UseVisualStyleBackColor = true;
            // 
            // chbWallWrap
            // 
            this.chbWallWrap.AutoSize = true;
            this.chbWallWrap.Checked = true;
            this.chbWallWrap.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbWallWrap.Location = new System.Drawing.Point(14, 180);
            this.chbWallWrap.Name = "chbWallWrap";
            this.chbWallWrap.Size = new System.Drawing.Size(73, 17);
            this.chbWallWrap.TabIndex = 24;
            this.chbWallWrap.Text = "Wall wrap";
            this.chbWallWrap.UseVisualStyleBackColor = true;
            // 
            // ANN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(854, 609);
            this.Controls.Add(this.chbWallWrap);
            this.Controls.Add(this.chbPull);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.lblSteps);
            this.Controls.Add(this.btnTest);
            this.Controls.Add(this.gridPanel);
            this.Controls.Add(this.btnBegin);
            this.Controls.Add(this.lblTournamentSelRate);
            this.Controls.Add(this.txtTournamentSelectionRate);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblCrossOverRate);
            this.Controls.Add(this.lblMutationRate);
            this.Controls.Add(this.txtCrossOverRate);
            this.Controls.Add(this.txtMutationRate);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtWeightBitLength);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtPopulationSize);
            this.Controls.Add(this.cmbMateSelection);
            this.Controls.Add(this.cmbAdultSelection);
            this.Name = "ANN";
            this.Text = "ANN";
            this.Load += new System.EventHandler(this.ANN_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblCrossOverRate;
        private System.Windows.Forms.Label lblMutationRate;
        private System.Windows.Forms.TextBox txtCrossOverRate;
        private System.Windows.Forms.TextBox txtMutationRate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtWeightBitLength;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPopulationSize;
        private System.Windows.Forms.ComboBox cmbMateSelection;
        private System.Windows.Forms.ComboBox cmbAdultSelection;
        private System.Windows.Forms.Label lblTournamentSelRate;
        public System.Windows.Forms.TextBox txtTournamentSelectionRate;
        private System.Windows.Forms.Button btnBegin;
        private System.Windows.Forms.Panel gridPanel;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.Label lblSteps;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.CheckBox chbPull;
        private System.Windows.Forms.CheckBox chbWallWrap;
    }
}