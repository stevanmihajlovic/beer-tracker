﻿using System;
using System.Collections.Generic;
using EvolutionAlgorithm.Adult_Selection;
using EvolutionAlgorithm.Parent_Selection;
using EvolutionAlgorithm.Phenotype;

namespace EvolutionAlgorithm
{
    public class Population
    {
        public Adult Adult;
        public List<GenotypeInterface> adults;
        public Enums.AdultSelection adultSelection;
        public List<double> averageFitness;
        public bool bestFound;
        public List<GenotypeInterface> children;

        public int generation;

        public List<double> maxFitness;
        public Parent Parent;
        public Enums.ParentSelection parentSelection;
        public int populationSize;
        public Enums.Phenotype problemType;
        public List<double> standardDeviation;

        public Population()
        {
            bestFound = false;
            children = new List<GenotypeInterface>();
            adults = new List<GenotypeInterface>();
            generation = 0;
            populationSize = 20;

            maxFitness = new List<double>();
            averageFitness = new List<double>();
            standardDeviation = new List<double>();
        }

        public Population(int geneSize, Enums.Phenotype type, Enums.AdultSelection adult, Enums.ParentSelection parent,
            int populationsize)
        {
            children = new List<GenotypeInterface>();
            adults = new List<GenotypeInterface>();
            generation = 0;
            bestFound = false;
            problemType = type;
            adultSelection = adult;
            parentSelection = parent;
            populationSize = populationsize;
            maxFitness = new List<double>();
            averageFitness = new List<double>();
            standardDeviation = new List<double>();


            switch (adultSelection)
            {
                case Enums.AdultSelection.Full:
                    {
                        Adult = new FullGenerationalReplacement();
                        break;
                    }
                case Enums.AdultSelection.Mixing:
                    {
                        Adult = new GenerationalMixing();
                        break;
                    }
                case Enums.AdultSelection.OverProduction:
                    {
                        Adult = new OverProduction();
                        break;
                    }
            }

            switch (parentSelection)
            {
                case Enums.ParentSelection.TournamentSelection:
                    {
                        Parent = new TournamentSelection();
                        break;
                    }
                case Enums.ParentSelection.FitnessProportionate:
                    {
                        Parent = new FitnessProportionate();
                        break;
                    }
                case Enums.ParentSelection.SigmaScaling:
                    {
                        Parent = new SigmaScaling();
                        break;
                    }
                case Enums.ParentSelection.RankSelection:
                    {
                        Parent = new RankSelection();
                        break;
                    }
            }
        }

        public Population(int weightBitLenght, Enums.AdultSelection adult, Enums.ParentSelection parent,
            int populationsize)
        {
            children = new List<GenotypeInterface>();
            adults = new List<GenotypeInterface>();
            generation = 0;
            bestFound = false;
            adultSelection = adult;
            problemType = Enums.Phenotype.ANN;
            parentSelection = parent;
            populationSize = populationsize;
            maxFitness = new List<double>();
            averageFitness = new List<double>();
            standardDeviation = new List<double>();

            int[] numbers = new int[3];
            numbers[0] = Program.form.agent.wallWrap ? 5 : 7;
            numbers[1] = 2;
            numbers[2] = 2;//novo


            for (int i = 0; i < populationsize; i++)
            {
                children.Add(new Ph_ANN(numbers, true, weightBitLenght));
            }

            switch (adultSelection)
            {
                case Enums.AdultSelection.Full:
                    {
                        Adult = new FullGenerationalReplacement();
                        break;
                    }
                case Enums.AdultSelection.Mixing:
                    {
                        Adult = new GenerationalMixing();
                        break;
                    }
                case Enums.AdultSelection.OverProduction:
                    {
                        Adult = new OverProduction();
                        break;
                    }
            }

            switch (parentSelection)
            {
                case Enums.ParentSelection.TournamentSelection:
                    {
                        Parent = new TournamentSelection();
                        break;
                    }
                case Enums.ParentSelection.FitnessProportionate:
                    {
                        Parent = new FitnessProportionate();
                        break;
                    }
                case Enums.ParentSelection.SigmaScaling:
                    {
                        Parent = new SigmaScaling();
                        break;
                    }
                case Enums.ParentSelection.RankSelection:
                    {
                        Parent = new RankSelection();
                        break;
                    }
            }

        }

        public void Reproduce()
        {
                generation++;
                UpdateFitness();
                Adult.Selection(this);
                Adult.MakeChildren(this);
        }

        public string Print()
        {
            var print = "Generation: " + generation + "\tBest: " + children[0].Print() + "\tBest fitness: " +
                        Math.Round(children[0].GetFitness(), 2)
                        + "\tavg fitness:" + Math.Round(averageFitness[generation - 1], 2) + "\tDeviation: " +
                        Math.Round(standardDeviation[generation - 1], 3) + "\n";
            return print;
        }

        public void UpdateFitness()
        {
            double temp = 0;
            var max = children[0].GetFitness();
            foreach (var item in children)
            {
                temp += item.GetFitness();
                if (item.GetFitness() > max)
                {
                    max = item.GetFitness();
                }
            }
            if (generation != averageFitness.Count)
            {
                averageFitness.Add(temp / children.Count);
                maxFitness.Add(max);
            }
            if (generation != standardDeviation.Count)
            {
                standardDeviation.Add(0);
            }

            foreach (var item in children)
            {
                standardDeviation[generation - 1] += Math.Pow(item.GetFitness() - averageFitness[generation - 1], 2);
                standardDeviation[generation - 1] = Math.Sqrt(standardDeviation[generation - 1] / children.Count);
            }
        }
    }
}