﻿using System.Linq;

namespace EvolutionAlgorithm.Adult_Selection
{
    internal class GenerationalMixing : Adult
    {
        public void Selection(Population population)
        {
            var combined =
                population.children.Concat(population.adults).OrderByDescending(item => item.GetFitness()).ToList();
            population.adults.Clear();
            for (var i = 0; i < population.populationSize; i++)
            {
                population.adults.Add(combined[i]);
            }
            combined.Clear();
        }

        public void MakeChildren(Population population)
        {
            population.children.Clear();
            population.adults = population.adults.OrderByDescending(item => item.GetFitness()).ToList();
            if (Program.elitism)
            {
                population.children.Add(population.adults[0]);
            }

            for (var i = 0; i < population.populationSize - (Program.elitism ? 1 : 0); i++)
            {
                var first = population.Parent.Selection(population);
                var second = population.Parent.Selection(population);
                var kids = first.CrossOver(second);
                kids[0].CalculateFitness();
                kids[1].CalculateFitness();
                population.children.Add(kids[kids[0].GetFitness() > kids[1].GetFitness() ? 0 : 1]);
            }
            population.children = population.children.OrderByDescending(item => item.GetFitness()).ToList();
        }
    }
}