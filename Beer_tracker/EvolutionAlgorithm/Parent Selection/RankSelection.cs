﻿using EvolutionAlgorithm.Phenotype;

namespace EvolutionAlgorithm.Parent_Selection
{
    internal class RankSelection : Parent
    {
        public GenotypeInterface Selection(Population population)
        {
            double min, max, sum = 0;
            min = max = population.adults[0].GetFitness();
            foreach (var item in population.adults)
            {
                if (min > item.GetFitness())
                {
                    min = item.GetFitness();
                }
                if (max < item.GetFitness())
                {
                    max = item.GetFitness();
                }
            }
            var i = 0;
            foreach (var item in population.adults)
            {
                item.SetFitness(min +
                                (max - min)*((population.adults.Count - i++)/(double) (population.populationSize - 1)));
                sum += item.GetFitness();
            }

            var randomNumber = Program.rnd.NextDouble()*sum;
            var j = 0;
            while (randomNumber > 0)
            {
                randomNumber -= population.adults[j].GetFitness();
                j++;
            }
            foreach (var item in population.adults)
            {
                item.CalculateFitness();
            }
            return population.adults[j < 1 ? 0 : j - 1];
        }
    }
}