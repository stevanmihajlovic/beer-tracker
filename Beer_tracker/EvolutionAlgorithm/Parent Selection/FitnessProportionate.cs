﻿using EvolutionAlgorithm.Phenotype;

namespace EvolutionAlgorithm.Parent_Selection
{
    internal class FitnessProportionate : Parent
    {
        public GenotypeInterface Selection(Population population)
        {
            double sum = 0;
            foreach (var item in population.adults)
            {
                sum += item.GetFitness();
            }
            var randomNumber = Program.rnd.NextDouble()*sum;

            var i = 0;
            while (randomNumber > 0)
            {
                randomNumber -= population.adults[i].GetFitness();
                i++;
            }
            return population.adults[i < 1 ? 0 : i - 1];
        }
    }
}