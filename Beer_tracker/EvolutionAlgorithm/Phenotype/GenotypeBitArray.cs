﻿using System.Collections;
using EvolutionAlgorithm.Phenotype;

namespace EvolutionAlgorithm
{
    public abstract class GenotypeBitArray : GenotypeInterface
    {
        public BitArray genes;

        public GenotypeBitArray()
        {
        }

        public GenotypeBitArray(int geneSize, bool randomize = true)
        {
            makeGenes(geneSize,randomize);
        }

        public void makeGenes(int geneSize, bool randomize = true)
        {
            var rnd = Program.rnd;
            genes = new BitArray(geneSize);
            if (!randomize) return;
            for (var i = 0; i < geneSize; i++)
            {
                genes.Set(i, rnd.Next(2) == 1 ? true : false);
            }
            CalculateFitness();
        }
        public abstract void CalculateFitness();
        public abstract GenotypeInterface New();

        public void Mutation()
        {
            var rnd = Program.rnd;
            for (var i = 0; i < genes.Length; i++)
            {
                if (rnd.NextDouble() < Program.mutationRate)
                {
                    genes.Set(i, !genes.Get(i));
                }
            }
        }

        public GenotypeInterface[] CrossOver(GenotypeInterface second)
        {
            GenotypeInterface first = this;
            var returnGenotypeBitArray = new GenotypeInterface[2];
            returnGenotypeBitArray[0] = first.New();
            returnGenotypeBitArray[1] = first.New();

            if (Program.rnd.NextDouble() > Program.crossOverRate)
            {
                for (var i = 0; i < ((GenotypeBitArray) first).genes.Length; i++)
                {
                    ((GenotypeBitArray) returnGenotypeBitArray[0]).genes.Set(i, ((GenotypeBitArray) first).genes.Get(i));
                    ((GenotypeBitArray) returnGenotypeBitArray[1]).genes.Set(i, ((GenotypeBitArray) second).genes.Get(i));
                }
                returnGenotypeBitArray[0].CalculateFitness();
                returnGenotypeBitArray[1].CalculateFitness();
                return returnGenotypeBitArray;
            }
            var change = Program.rnd.Next(((GenotypeBitArray) first).genes.Length);

            for (var i = 0; i < ((GenotypeBitArray) first).genes.Length; i++)
            {
                if (i < change)
                {
                    ((GenotypeBitArray) returnGenotypeBitArray[0]).genes.Set(i, ((GenotypeBitArray) first).genes.Get(i));
                    ((GenotypeBitArray) returnGenotypeBitArray[1]).genes.Set(i, ((GenotypeBitArray) second).genes.Get(i));
                }
                else
                {
                    ((GenotypeBitArray) returnGenotypeBitArray[0]).genes.Set(i, ((GenotypeBitArray) second).genes.Get(i));
                    ((GenotypeBitArray) returnGenotypeBitArray[1]).genes.Set(i, ((GenotypeBitArray) first).genes.Get(i));
                }
            }
            returnGenotypeBitArray[0].Mutation();
            returnGenotypeBitArray[1].Mutation();
            returnGenotypeBitArray[0].CalculateFitness();
            returnGenotypeBitArray[1].CalculateFitness();
            return returnGenotypeBitArray;
        }

        public string Print()
        {
            var print = "";
            foreach (bool i in genes)
            {
                if (i)
                {
                    print += "1";
                }
                else
                {
                    print += "0";
                }
            }
            return print;
        }

        public abstract double GetFitness();
        public abstract void SetFitness(double newFitness);
    }
}