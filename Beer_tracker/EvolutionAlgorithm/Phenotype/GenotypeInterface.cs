﻿namespace EvolutionAlgorithm.Phenotype
{
    public interface GenotypeInterface
    {
        void CalculateFitness();
        void Mutation();
        GenotypeInterface[] CrossOver(GenotypeInterface second);
        GenotypeInterface New();
        double GetFitness();
        string Print();
        void SetFitness(double newFitness);
    }
}