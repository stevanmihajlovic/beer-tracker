﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvolutionAlgorithm.Phenotype
{
    public class Ph_ANN : GenotypeBitArray
    {
        public double fitness;
        public int doubleLength;
        public double[] arrayOfWeights;
        public Layer[] layers;

        public Ph_ANN(int[] layer, bool randomize = true, int DoubleLength = 10)
        {
            layers = new Layer[layer.Length];
            for (int i = 0; i < layers.Length; i++)
            {
                layers[i] = new Layer(layer[i]);
                for (int j = 0; j < layer[i]; j++)
                {
                    layers[i].nodes[j] = new Node();
                }
            }
            doubleLength = DoubleLength;

            int numberOfWeights = 0;
            for (int i = 1; i < layers.Length; i++)
            {//regular weights
                numberOfWeights = numberOfWeights + layers[i-1].nodes.Length * layers[i].nodes.Length; //input weights
                numberOfWeights = numberOfWeights + layers[i].nodes.Length * 3; //g, t, bias
                numberOfWeights = numberOfWeights + layers[i].nodes.Length * layers[i].nodes.Length; //interconnecting weights
            }
            makeGenes(numberOfWeights * doubleLength, randomize);

            if (randomize)
                updateWeights();
        }

        public void updateWeights()
        {
            int k;
            arrayOfWeights = new double[genes.Length / doubleLength];
            for (int i = 0; i < arrayOfWeights.Length; i++)
            {
                double value = 0;
                k = 1;
                for (int j = 0; j < doubleLength; j++)
                {
                    if (genes.Get(i * doubleLength + j))
                    {
                        value += k;
                    }
                    k = k << 1;
                }
                arrayOfWeights[i] = value / ((double)k - 1);
            }
            int counter = arrayOfWeights.Length - 1;
            for (int i = 1; i < layers.Length; i++)
            {
                for (int j = 0; j < layers[i].nodes.Length; j++)
                {
                    layers[i].nodes[j].t = arrayOfWeights[counter--] + 1;
                    layers[i].nodes[j].g = arrayOfWeights[counter--] * 4 + 1;
                }
            }


        }
        public override void CalculateFitness()
        {
            Program.form.agent.weights = this;
            this.updateWeights();
            fitness = Program.form.agent.calculateFitness();
        }

        public override double GetFitness()
        {
            return fitness;
        }

        public override GenotypeInterface New()
        {
            int[] numbers = new int[layers.Length];
            for (int i = 0; i < numbers.Length; i++)
            {
                numbers[i] = layers[i].nodes.Length;
                for (int j = 0; j < layers[i].nodes.Length; j++)
                {
                    layers[i].nodes[j] = new Node();
                }
            }
            GenotypeInterface returnGenotypeInterface = new Ph_ANN(numbers, false, this.doubleLength);
            return returnGenotypeInterface;
        }

        public override void SetFitness(double newFitness)
        {
            fitness = newFitness;
        }
    }
}
